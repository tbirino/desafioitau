package br.com.itau.desafioitau

import br.com.itau.desafioitau.dto.UsuarioDTO
import org.junit.jupiter.api.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpStatus


@SpringBootTest(
    classes = [DesafioitauApplication::class], webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
class DesafioitauApplicationTests {

    @Autowired
    lateinit var restTemplate: TestRestTemplate

    @Test
    @Order(1)
    fun criar() {
        val dto = UsuarioDTO(nome = "Thales Birino", cpf = "02382666137")
        val result = restTemplate.postForEntity(
            "/usuario",
            dto,
            UsuarioDTO::class.java
        )
        Assertions.assertEquals(result.statusCode, HttpStatus.OK)
    }

    @Test
    @Order(2)
    fun editar() {
        assertDoesNotThrow {
            val dto = UsuarioDTO(id = 1, nome = "Thales Carvalho Birino", cpf = "02382666137")
            restTemplate.put(
                "/usuario",
                dto,
                UsuarioDTO::class.java
            )
        }
    }


    @Test
    @Order(3)
    fun consultar() {
        val result = restTemplate.getForEntity("/usuario", List::class.java)
    }

}
