package br.com.itau.desafioitau.service

import br.com.itau.desafioitau.dto.UsuarioDTO
import br.com.itau.desafioitau.entity.Usuario
import br.com.itau.desafioitau.exception.UsuarioExistenteException
import br.com.itau.desafioitau.repository.UsuarioRepository
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import java.util.*

internal class UsuarioServiceTest {
    val usuarioRepository: UsuarioRepository = mockk()
    val usuarioService = UsuarioService(usuarioRepository)
    val usuario = Usuario(
        id = 0L,
        nome = "Thales Birino",
        cpf = "02382666137"
    )
    val usuarioDTO = UsuarioDTO(
        id = 0L,
        nome = "Thales Birino",
        cpf = "02382666137"
    )


    @Test
    fun criar() {
        every { usuarioRepository.findByCpf(any()) } returns Optional.empty()
        every { usuarioRepository.save(any()) } returns usuario
        val usuarioSalvo = usuarioService.criar(usuarioDTO)
        assertEquals(usuarioSalvo.cpf, usuario.cpf)
    }

    @Test
    fun criarException() {
        every { usuarioRepository.findByCpf(any()) } returns Optional.of(usuario)
        every { usuarioRepository.save(any()) } returns usuario
        assertThrows(
            UsuarioExistenteException::class.java,
            { usuarioService.criar(usuarioDTO) }, "Usuario já existe!"
        )
    }

    @Test
    fun editar() {
        val usuario = Usuario(id = 1L, nome = "Thales Birino", "02382666137")
        every { usuarioRepository.findByCpf(any()) } returns Optional.empty()
        every { usuarioRepository.save(any()) } returns usuario
        val usuarioSalvo = usuarioService.editar(UsuarioDTO(id = 1L, nome = "Thales Carvalho", cpf = "37333046168"))
        assertEquals(usuarioSalvo!!.id, usuario.id)
    }

    @Test
    fun consultar() {
        val listOfUsuario = listOf(usuario)
        every { usuarioRepository.findAll() } returns listOfUsuario
        val resultado = usuarioService.consultar()
        assert(resultado.isNotEmpty())
        assertEquals(resultado.first().id, listOfUsuario.first().id)
    }
}
