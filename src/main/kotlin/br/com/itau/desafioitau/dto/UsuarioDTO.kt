package br.com.itau.desafioitau.dto

data class UsuarioDTO(val id: Long? = null, val nome: String, val cpf: String)
