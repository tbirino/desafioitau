package br.com.itau.desafioitau.service

import br.com.itau.desafioitau.dto.UsuarioDTO
import br.com.itau.desafioitau.entity.Usuario
import br.com.itau.desafioitau.exception.UsuarioExistenteException
import br.com.itau.desafioitau.repository.UsuarioRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional

@Service
class UsuarioService(@Autowired val usuarioRepository: UsuarioRepository) {

    @Transactional
    fun criar(dto: UsuarioDTO): UsuarioDTO {
        validarSeUsuarioExiste(dto)
        val usuario = usuarioRepository.save(Usuario(id = 0L, nome = dto.nome!!, cpf = dto.cpf!!))
        return UsuarioDTO(id = usuario.id, nome = usuario.nome, usuario.cpf)
    }

    @Transactional
    fun editar(dto: UsuarioDTO): UsuarioDTO? {
        val usuario = usuarioRepository.save(Usuario(id = dto.id!!, nome = dto.nome!!, cpf = dto.cpf!!))
        return UsuarioDTO(id = usuario.id, nome = usuario.nome, cpf = usuario.cpf)
    }

    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    fun consultar(): List<UsuarioDTO> {
        val usuarios = usuarioRepository.findAll()
        return usuarios.map { UsuarioDTO(id = it.id, nome = it.nome, cpf = it.cpf) }
    }

    private fun validarSeUsuarioExiste(dto: UsuarioDTO) {
        usuarioRepository.findByCpf(dto.cpf!!).ifPresent { throw UsuarioExistenteException("Usuario já existe!") }
    }


}
