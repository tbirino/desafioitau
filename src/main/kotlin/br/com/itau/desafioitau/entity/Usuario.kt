package br.com.itau.desafioitau.entity

import javax.persistence.*

@Entity
@Table(name = "USUARIO")
@SequenceGenerator(name = "ID_GENERATOR_USUARIO", sequenceName = "SEQ_USUARIO", allocationSize = 1)
class Usuario(
        @Id
        @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ID_GENERATOR_USUARIO")
        @Column(name = "ID")
        var id: Long,

        @Column(name = "NOME", nullable = false, length = 120)
        val nome: String,

        @Column(name = "CPF", nullable = false, length = 14)
        val cpf: String,

        )

