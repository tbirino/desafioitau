package br.com.itau.desafioitau.controller

import br.com.itau.desafioitau.dto.UsuarioDTO
import br.com.itau.desafioitau.service.UsuarioService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("usuario")
class UsuarioController {

    @Autowired
    private lateinit var usuarioService: UsuarioService

    @PostMapping
    fun criar(@RequestBody usuario: UsuarioDTO): ResponseEntity<UsuarioDTO> {
        val usuario = usuarioService.criar(usuario)
        return ResponseEntity.ok(usuario)
    }

    @PutMapping
    fun editar(@RequestBody form: UsuarioDTO): ResponseEntity<UsuarioDTO> {
        val filiadoDtoEditado = usuarioService.editar(form)
        return ResponseEntity.ok(filiadoDtoEditado)
    }

    @GetMapping
    fun consultar(): ResponseEntity<List<UsuarioDTO>> {
        val consultar = usuarioService.consultar()
        return ResponseEntity.ok(consultar)
    }


}
