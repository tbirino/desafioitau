package br.com.itau.desafioitau.repository

import br.com.itau.desafioitau.entity.Usuario
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.util.Optional

@Repository
interface UsuarioRepository : CrudRepository<Usuario, Long> {
    fun findByCpf(@Param("cpf") cpf: String): Optional<Usuario>
}
