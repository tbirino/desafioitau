package br.com.itau.desafioitau.exception

class UsuarioExistenteException(message: String?): RuntimeException(message) {
}
